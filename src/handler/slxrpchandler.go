package handler

import (
	"bytes"
	"encoding/json"
	"entity"
	"io/ioutil"
	"log"
	"net/http"
)

//SlxRPCStubLiveCheck : Live Check
func SlxRPCStubLiveCheck() bool {
	res, err := http.Get("http://localhost:5000/api/rpcstub/livecheck")
	handleError(err)

	body, err := ioutil.ReadAll(res.Body)
	handleError(err)

	if res.Status == "200 OK" && string(body) == "PNE RPC Stub is Live" {
		return true
	}

	return false
}

//SlxRPCStubCall : Initiate RPC
func SlxRPCStubCall(message entity.RPCRelayMessage) interface{} {
	messageJSON, err := json.Marshal(message)
	handleError(err)

	responseBody := bytes.NewBuffer(messageJSON)

	res, err := http.Post("http://localhost:5000/api/rpcstub/call", "application/json", responseBody)
	handleError(err)

	if res.Status == "200 OK" {
		body, err := ioutil.ReadAll(res.Body)
		handleError(err)

		var result interface{}

		err = json.Unmarshal(body, &result)
		handleError(err)

		return result
	}

	return nil
}

func handleError(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}
