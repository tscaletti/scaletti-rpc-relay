package server

import (
	"context"
	"entity"
	"fmt"
	"handler"
	"log"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

type displayNameServiceServer struct{}

//LaunchServer :
func LaunchServer() {
	lis, err := net.Listen("tcp", ":5005")

	if err != nil {
		log.Fatalln(err)
	}

	grpcServer := grpc.NewServer()

	entity.RegisterDisplayNameServiceServer(grpcServer, &displayNameServiceServer{})

	reflection.Register(grpcServer)

	fmt.Println("RPC Relay Listening at Port 5005...")

	if event := grpcServer.Serve(lis); event != nil {
		panic(event)
	}
}

func (*displayNameServiceServer) GetDisplayName(ctx context.Context, request *entity.Request) (*entity.Response, error) {
	//Live Check
	pneIsAlive := handler.SlxRPCStubLiveCheck()

	if !pneIsAlive {
		panic("PNE is dead!!")
	}

	message := entity.RPCRelayMessage{
		ClassType:    request.GetClasstype(),
		FunctionName: request.GetFunctionName(),
	}

	for _, param := range request.GetFunctionParameters() {
		message.FunctionParameters = append(message.FunctionParameters, param)
	}

	fmt.Println(message)

	//Initiate RPC
	displayName := handler.SlxRPCStubCall(message)

	if displayName == nil {
		panic("Failed on PNE communication")
	}

	result := entity.Response{
		Result: displayName.(string),
	}

	return &result, nil
}
